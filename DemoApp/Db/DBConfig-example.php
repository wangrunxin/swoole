<?php
namespace App\Db;

use EasySwoole\Component\Singleton;
use EasySwoole\ORM\Db\Config;
use wrxswoole\Core\Database\BaseDBConfig;

/**
 *
 * @author WANG RUNXIN
 *        
 */
class DBConfig
{

    const CONNECTION_WRITE = "default";

    use BaseDBConfig;
    use Singleton;

    static function getConfig($connectionName): Config
    {
        $config = self::initConfig();

        switch ($connectionName) {
            default:
                $config->setDatabase('*');
                $config->setUser('*');
                $config->setPassword('*');
                $config->setHost('*');
                break;
        }

        return $config;
    }

    function loadExtDB()
    {}
}

?>